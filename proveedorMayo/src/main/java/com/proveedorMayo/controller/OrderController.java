package com.proveedorMayo.controller;

import com.proveedorMayo.dto.ProductDto;
import com.proveedorMayo.service.OrderService;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;


@Controller
@RequestMapping("/orderController")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value="/send", method = RequestMethod.POST)
    public @ResponseBody
    String send(WebRequest request){

        Map data = (Map) JSONValue.parse(request.getParameter("data"));
        String personalId = (String)((JSONObject)data.get("user")).get("id");
        List<ProductDto> dtos = new ArrayList<ProductDto>();
        Map mapOrder = (Map)((JSONObject)data.get("order"));
        Iterator it = mapOrder.keySet().iterator();
        while(it.hasNext()){
            Object key = it.next();
            ProductDto dto = new ProductDto();
            Map productMap1 = (Map)mapOrder.get(key);
            dto.setAmount(new Long((String) productMap1.get("amount")));
            Map productMap2 = (Map)productMap1.get("product");
            dto.setName((String) productMap2.get("name"));
            dto.setInternalCode((String) productMap2.get("internalCode"));
            dto.setId((Number) productMap2.get("id"));
            dto.setTotal(new BigDecimal((String) productMap1.get("total")));
            dto.setPrice(new BigDecimal((String) productMap2.get("price")));
            dtos.add(dto);
        }
        orderService.sendOrder(dtos,personalId);
        return null;
    }

}
