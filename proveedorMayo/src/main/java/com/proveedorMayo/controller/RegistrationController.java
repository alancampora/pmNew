package com.proveedorMayo.controller;

import com.proveedorMayo.domain.UserCompany;
import com.proveedorMayo.domain.UserPerson;
import com.proveedorMayo.service.UserCompanyService;
import com.proveedorMayo.service.UserPersonService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@Controller
@RequestMapping("/registrationController")
public class RegistrationController {

    @Autowired
    private UserPersonService userPersonService;

    @Autowired
    private UserCompanyService userCompanyService;

    @RequestMapping(value="/person", method = RequestMethod.POST)
    public @ResponseBody String person(WebRequest request){

        JSONObject obj = new JSONObject();
        Map json = (Map)JSONValue.parse(request.getParameter("value"));
        UserPerson userPerson = new UserPerson();
        userPerson.setSurname((String) json.get("surname"));
        userPerson.setPassword((String) json.get("password"));
        userPerson.setEmail((String) json.get("email"));
        userPerson.setPersonalId(((Number) json.get("personalId")).toString());
        userPerson.setAddress((String) json.get("address"));
        userPerson.setNeighborhood((String) json.get("neighborhood"));
        userPerson.setPostalCode((String) json.get("postalCode"));
        userPerson.setName((String) json.get("name"));
        userPerson.setPhone((String) json.get("phone"));

        UserPerson find = userPersonService.findUserByPersonalId(userPerson.getPersonalId());
        if (find == null){
            userPersonService.save(userPerson);
            obj.put("success", true);
            obj.put("status", "Usuario registrado correctamente");
        }else{
            obj.put("success", false);
            obj.put("status", "Usuario ya registrado");
        }

        return obj.toString();
    }

    @RequestMapping(value="/company", method = RequestMethod.POST)
    public @ResponseBody String company(WebRequest request){

        JSONObject obj = new JSONObject();
        Map json = (Map)JSONValue.parse(request.getParameter("value"));
        UserCompany userCompany = new UserCompany();
        userCompany.setPassword((String) json.get("password"));
        userCompany.setEmail((String) json.get("email"));
        userCompany.setCompanyName((String) json.get("companyName"));
        userCompany.setCompanyId(((Number) json.get("companyId")).toString());
        userCompany.setAddress((String) json.get("address"));
        userCompany.setNeighborhood((String) json.get("neighborhood"));
        userCompany.setPostalCode((String) json.get("postalCode"));
        userCompany.setPhone((String)json.get("phone"));

        UserCompany find = userCompanyService.findUserByCompanyId(userCompany.getCompanyId());
        if (find == null){
            userCompanyService.save(userCompany);
            obj.put("success", true);
            obj.put("status", "Usuario registrado correctamente");
        }else{
            obj.put("success", false);
            obj.put("status", "Usuario ya registrado");
        }

        return obj.toString();
    }

}
