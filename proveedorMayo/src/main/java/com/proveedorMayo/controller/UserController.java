package com.proveedorMayo.controller;

import com.proveedorMayo.domain.UserPerson;
import com.proveedorMayo.service.UserPersonService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

@Controller
@RequestMapping("/userController")
public class UserController {

    @Autowired
    private UserPersonService userService;

    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public @ResponseBody String login(WebRequest request) {

        JSONObject obj = new JSONObject();
        UserPerson user = new UserPerson();

        try {
            userService.save(user);
            obj.put("status", "success");
            obj.put("userId", user.getId());
            obj.put("info", "Registrado");
        }catch (Exception e){
            obj.put("status", "failed");
            obj.put("info", "Intente de nuevo");
        }

        return obj.toString();
    }
}
