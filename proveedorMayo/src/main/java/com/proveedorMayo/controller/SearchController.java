package com.proveedorMayo.controller;

import com.proveedorMayo.domain.Product;
import com.proveedorMayo.service.MailService;
import com.proveedorMayo.service.SearchService;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import java.util.List;

@Controller
@RequestMapping("/searchController")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @Autowired
    private MailService mailService;

    @RequestMapping(value="/search", method = RequestMethod.GET)
    public @ResponseBody String search(WebRequest request){

        JSONArray arr = new JSONArray();
        List<Product> listProduct = null;
        String value = request.getParameter("value");
        if (value != null){
            listProduct = searchService.findProduct(value.toUpperCase());
        }else{
            listProduct = searchService.findAll();
        }
        for (Product p: listProduct){
            JSONObject obj = new JSONObject();

            obj.put("id", p.getId());
            obj.put("internalCode", p.getInternalCode());
            obj.put("name" , p.getName());
            obj.put("price", p.getPrice());

            arr.add(obj);
        }

        return arr.toJSONString();
    }

}
