package com.proveedorMayo.controller;

import com.proveedorMayo.domain.UserCompany;
import com.proveedorMayo.domain.UserPerson;
import com.proveedorMayo.service.UserCompanyService;
import com.proveedorMayo.service.UserPersonService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

@Controller
@RequestMapping("/loginController")
public class LoginController {

    @Autowired
    private UserPersonService userService;
    @Autowired
    private UserCompanyService userCompanyService;
    
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public @ResponseBody String login(WebRequest request){

        String personalId = request.getParameter("personalId");
        String pass = request.getParameter("password");
        UserPerson up = userService.findUser(personalId, pass);
        JSONObject obj = new JSONObject();
        if (up != null){
            obj.put("status", "success");
            obj.put("success" , true);
            obj.put("userId", up.getId());
            obj.put("name", up.getSurname() + ", " + up.getName());
            obj.put("id", up.getPersonalId());
            obj.put("info", "Bienvenido");
        }else{
            UserCompany uc = userCompanyService.findUser(personalId, pass);
            if (uc != null){
                obj.put("status", "success");
                obj.put("success" , true);
                obj.put("userId", uc.getId());
                obj.put("name", uc.getCompanyName());
                obj.put("id", uc.getCompanyId());
                obj.put("info", "Bienvenido");
            }else{
                obj.put("status", "fail");
                obj.put("success" , false);
                obj.put("info", "No se encuentra registrado");
            }
        }

        return obj.toString();
    }
}
