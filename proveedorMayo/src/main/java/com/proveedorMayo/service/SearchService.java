package com.proveedorMayo.service;

import com.proveedorMayo.domain.Product;
import com.proveedorMayo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> findProduct(String value){
        return (List<Product>) productRepository.findProduct(value);
    }

    public List<Product> findAll(){
        return (List<Product>) productRepository.findAll();
    }
}
