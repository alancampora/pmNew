/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.service;

import com.proveedorMayo.domain.UserPerson;
import com.proveedorMayo.repository.UserPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserPersonService {

    @Autowired
    private UserPersonRepository userPersonRepository;

    public void save(UserPerson user){
        userPersonRepository.save(user);
    }

    public UserPerson findUser(String personalId, String pass){
        return userPersonRepository.findUser(personalId, pass);
    }

    public UserPerson findUserByPersonalId(String personalId){
        return userPersonRepository.findUserByPersonalId(personalId);
    }

    public UserPerson findUserId(Long id){
        return userPersonRepository.findOne(id);
    }
    
}
