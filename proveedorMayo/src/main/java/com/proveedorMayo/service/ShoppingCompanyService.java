package com.proveedorMayo.service;

import com.proveedorMayo.domain.ShoppingCompany;
import com.proveedorMayo.domain.ShoppingPerson;
import com.proveedorMayo.repository.ShoppingCompanyRepository;
import com.proveedorMayo.repository.ShoppingPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCompanyService {

    @Autowired
    private ShoppingCompanyRepository shoppingCompanyRepository;

    public void save(ShoppingCompany shoppingCompany){
        shoppingCompanyRepository.save(shoppingCompany);
    }
}
