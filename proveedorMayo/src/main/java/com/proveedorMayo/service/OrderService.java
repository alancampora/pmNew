package com.proveedorMayo.service;

import com.proveedorMayo.domain.Product;
import com.proveedorMayo.domain.ShoppingPerson;
import com.proveedorMayo.domain.UserPerson;
import com.proveedorMayo.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrderService {

    @Autowired
    private MailService mailService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UserPersonService userPersonService;
    @Autowired
    private ShoppingPersonService shoppingPersonService;

    public void sendOrder(List<ProductDto> listDto, String personalId){
        UserPerson userPerson = userPersonService.findUserByPersonalId(personalId);
        Set<Product> setProd = new HashSet<Product>();
        for (ProductDto dto : listDto){
            Product p = productService.findById(dto.getId().longValue());
            setProd.add(p);
        }
        ShoppingPerson sp = new ShoppingPerson();
        sp.setIdUserPerson(userPerson);
        sp.setDateShopping(new Date());
        sp.setProducts(setProd);
        shoppingPersonService.save(sp);
        try {
            mailService.sendMail(userPerson,listDto);
        }catch (RuntimeException e){
            System.out.println(e.getMessage());
        }

    }

}
