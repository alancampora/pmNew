/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.service;

import com.proveedorMayo.domain.UserPerson;
import com.proveedorMayo.dto.ProductDto;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Properties;


@Service
public class MailService {

    private final String username = "ventasdistribuidoramayo@gmail.com";
    private final String password = "kkcxojyopsaciflz";

    public void sendMail(UserPerson user, List<ProductDto> products){

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ventasdistribuidoramayo@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("alan.campora@gmail.com"));
            message.setSubject("Pedido del cliente: " + user.getSurname() + ", " + user.getName());
            BigDecimal totalFinal = new BigDecimal(0);
            StringBuffer sb = new StringBuffer();
            sb.append("Articulos: \n");
            for (ProductDto dto : products){
                BigDecimal j = new BigDecimal(0);
                sb.append("     Nombre articulo: " + dto.getName() + "\n");
                sb.append("     Precio: " + dto.getPrice() + "\n");
                sb.append("     Cantidad: " + dto.getAmount() + "\n");
                sb.append("     Total por articulo:" + dto.getTotal().setScale(2, RoundingMode.HALF_UP) + "\n");
                totalFinal = totalFinal.add(dto.getTotal());
            }
            totalFinal = totalFinal.setScale(2, RoundingMode.HALF_UP);
            sb.append("\n Total Final: " + totalFinal);
            message.setText(sb.toString());

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    
}
