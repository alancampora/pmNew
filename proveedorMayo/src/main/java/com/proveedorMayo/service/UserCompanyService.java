/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.service;

import com.proveedorMayo.domain.UserCompany;
import com.proveedorMayo.domain.UserPerson;
import com.proveedorMayo.repository.UserCompanyRepository;
import com.proveedorMayo.repository.UserPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserCompanyService {

    @Autowired
    private UserCompanyRepository userCompanyRepository;

    public void save(UserCompany user){
        userCompanyRepository.save(user);
    }

    public UserCompany findUser(String personalId, String pass){
        return userCompanyRepository.findUser(personalId, pass);
    }

    public UserCompany findUserByCompanyId(String companyId){
        return userCompanyRepository.findUserByCompanyId(companyId);
    }
    
}
