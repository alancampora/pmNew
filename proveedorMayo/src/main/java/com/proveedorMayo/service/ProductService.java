package com.proveedorMayo.service;

import com.proveedorMayo.domain.Product;
import com.proveedorMayo.repository.ProductRepository;
import com.proveedorMayo.repository.UserCompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product findById(Long id){
        return productRepository.findOne(id);
    }
}
