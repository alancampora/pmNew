package com.proveedorMayo.service;

import com.proveedorMayo.domain.Product;
import com.proveedorMayo.domain.ShoppingPerson;
import com.proveedorMayo.repository.ProductRepository;
import com.proveedorMayo.repository.ShoppingPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShoppingPersonService {

    @Autowired
    private ShoppingPersonRepository shoppingPersonRepository;

    public void save(ShoppingPerson shoppingPerson){
        shoppingPersonRepository.save(shoppingPerson);
    }
}
