/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.repository;

import com.proveedorMayo.domain.UserPerson;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserPersonRepository extends CrudRepository<UserPerson, Long> {

    @Query("select up from UserPerson up where up.personalId = ?1 and up.password = ?2")
    UserPerson findUser(String personalId, String pass);

    @Query("select up from UserPerson up where up.personalId = ?1")
    UserPerson findUserByPersonalId(String personalId);
    
}
