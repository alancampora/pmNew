/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.repository;

import com.proveedorMayo.domain.UserCompany;
import com.proveedorMayo.domain.UserPerson;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserCompanyRepository extends CrudRepository<UserCompany, Long> {

    @Query("select uc from UserCompany uc where uc.companyId = ?1 and uc.password = ?2")
    UserCompany findUser(String companyId, String pass);

    @Query("select uc from UserCompany uc where uc.companyId = ?1")
    UserCompany findUserByCompanyId(String companyId);
}
