/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.repository;

import com.proveedorMayo.domain.ShoppingPerson;
import com.proveedorMayo.domain.UserPerson;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ShoppingPersonRepository extends CrudRepository<ShoppingPerson, Long> {
    
}
