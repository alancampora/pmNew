/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.repository;

import com.proveedorMayo.domain.ShoppingCompany;
import org.springframework.data.repository.CrudRepository;

public interface ShoppingCompanyRepository extends CrudRepository<ShoppingCompany, Long> {
    
}
