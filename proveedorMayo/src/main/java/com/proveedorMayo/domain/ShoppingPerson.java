/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class ShoppingPerson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date dateShopping;
    @OneToOne
    private UserPerson idUserPerson;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateShopping() {
        return dateShopping;
    }

    public void setDateShopping(Date dateShopping) {
        this.dateShopping = dateShopping;
    }

    public UserPerson getIdUserPerson() {
        return idUserPerson;
    }

    public void setIdUserPerson(UserPerson idUserPerson) {
        this.idUserPerson = idUserPerson;
    }

    @ManyToMany
    @JoinTable(name="shopping_person_product",
            joinColumns=@JoinColumn(name="ID_SHOPPING_PERSON"),
            inverseJoinColumns=@JoinColumn(name="ID_PRODUCT"))
    private Set<Product> products = new HashSet<Product>();

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

}