/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proveedorMayo.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ShoppingCompany {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date dateShopping;
    @OneToOne
    private UserCompany idUserCompany;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateShopping() {
        return dateShopping;
    }

    public void setDateShopping(Date dateShopping) {
        this.dateShopping = dateShopping;
    }

    public UserCompany getIdUserCompany() {
        return idUserCompany;
    }

    public void setIdUserCompany(UserCompany idUserCompany) {
        this.idUserCompany = idUserCompany;
    }
}