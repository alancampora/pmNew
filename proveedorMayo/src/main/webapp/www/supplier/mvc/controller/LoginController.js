(function(){
    'use strict';

    angular
        .module('app')
        .controller('LoginController', ['$scope','AuthenticationService','$state', function($scope,AuthenticationService,$state){

            $scope.user = "";
            $scope.password = "";
            $scope.showModal = false;
            $scope.message = null;

            $scope.login = function login() {

                console.log($scope.user + " " + $scope.password);

                return AuthenticationService.login($scope.user,$scope.password,
                    //success
                    function(data){

                        //if(data.success) {
                        //    $scope.showModal = true;
                        //
                        //    $scope.message = "Bienvenido" + data.name;
                        //
                        //    //Go to user page and catalog
                        //    $state.go('user.catalog');
                        //
                        //}else{
                        //    $scope.showModal = true;
                        //
                        //    $scope.message = "Error al logearse" + data.name;
                        //}


                        if(data.success){

                            $scope.showModal = true;
                            $scope.message = "Bienvenido" + data.name;

                            //alert("User success: " + data.name);


                            //Go to user page and catalog
                            setTimeout(function(){
                                $state.go('user.catalog');
                            },2000);

                        }else{
                            alert("User failed: " + data.name);
                        }

                    },
                    //fail
                    function(data){
                        alert("User fail: " + data);
                    })
            };

        }]);
})();

