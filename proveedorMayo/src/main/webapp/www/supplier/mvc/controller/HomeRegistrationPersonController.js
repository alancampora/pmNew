(function(){
    'use strict';

    angular
        .module('app')
        .controller('HomeRegistrationPersonController',['$scope','$state','RegistrationService', function($scope,$state,RegistrationService){

            $scope.data = {
                username:null,
                password:null,
                surname:null,
                email:null,
                personalId:null,
                address:null,
                neighborhood:null,
                postalCode:null,
                phone:null
            };

            $scope.showModal = false;

            $scope.message = null;

            $scope.register = function register (){

                //Check form
                if($scope.userForm.$invalid){
                    $scope.showModal = true;
                    $scope.message = "Complete los datos faltantes";
                }else{
                    RegistrationService.registerPerson($scope.data,
                        //success
                        function(data){
                            $scope.showModal = true;

                            $scope.message = data.status;

                            setTimeout(function(){
                                $scope.showModal = false;
                                    },2000);

                        },
                        //fail
                        function(){

                        }
                    );
                }


            };

        }]);


})()