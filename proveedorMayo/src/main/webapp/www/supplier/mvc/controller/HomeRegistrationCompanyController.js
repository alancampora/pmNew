(function(){
    'use strict';

    angular
        .module('app')
        .controller('HomeRegistrationCompanyController',['$scope','$state','RegistrationService', function($scope,$state,RegistrationService){

            $scope.data = {
                companyId:null,
                companyName:null,
                password:null,
                email:null,
                address:null,
                neighborhood:null,
                postalCode:null,
                phone:null
            };

            $scope.message = null;

            $scope.register = function register (){

                //Check form
                if($scope.companyForm.$invalid){
                    $scope.showModal = true;
                    $scope.message = "Complete los datos faltantes";
                }else{
                    RegistrationService.registerCompany($scope.data,
                        //success
                        function(data){

                            $scope.showModal = true;

                            $scope.message = data.status;

                            setTimeout(function () {
                                $scope.showModal = false;
                            }, 2000);

                        },
                        //fail
                        function(){

                        }
                    );
                }


            };

        }]);


})()