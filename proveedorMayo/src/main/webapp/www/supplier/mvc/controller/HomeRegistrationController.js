(function(){
    'use strict';

    angular
        .module('app')
        .controller('HomeRegistrationController',
        ['$scope','RegistrationService','$state', function($scope,AuthService,$state){


            $scope.init = function(){
                //Init Shopping Cart service
                ShoppingCartService.init();

            };


        }]);


})()