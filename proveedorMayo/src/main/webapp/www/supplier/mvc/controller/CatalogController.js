(function(){
    'use strict';

    angular
        .module('app')
        .controller('CatalogController', ['$scope','SearchService','ShoppingCartService', function($scope,SearchService,ShoppingCartService){

            //User's value search
            $scope.valueSearch = null;

            //Results after search
            $scope.products = null;

            $scope.addProductSuccess = false;


            $scope.search = function search() {

                console.log("Executing Search");

                return SearchService.search($scope.valueSearch,
                    //success
                    function(data){
                        //alert("User success: " + data);

                        $scope.showProducts(data);
                        //show products to select
                    },
                    //fail
                    function(data){
                        alert("User fail: " + data);
                    })
            };


            $scope.showProducts = function showProducts (data){
                //Show products and that's all from here - Its going to be used by the directive
                $scope.products = data;

            };

            $scope.addProduct = function addProduct(product,amount){
                console.log("Product:" +JSON.stringify(product) +" amount:" + amount + " total:" +  product.price * amount);
                ShoppingCartService.addProduct(product,amount);

                //Show alert
                $scope.addProductSuccess = true;
            }


        }]);


})()