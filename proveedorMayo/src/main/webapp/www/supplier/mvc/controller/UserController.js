(function(){
    'use strict';

    angular
        .module('app')
        .controller('UserController',
        ['$scope','AuthenticationService','$state','ShoppingCartService', function($scope,AuthService,$state,ShoppingCartService){

            $scope.init = function(){
                //Init Shopping Cart service
                ShoppingCartService.init();

            };

            $scope.logout = function(){
                AuthService.logout();
                $state.go('home.main');
            };

        }]);


})()