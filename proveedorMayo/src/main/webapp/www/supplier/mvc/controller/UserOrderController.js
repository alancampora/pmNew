(function(){
    'use strict';

    angular
        .module('app')
        .controller('UserOrderController',
        ['$scope','UserOrderService','$state','ShoppingCartService','AuthenticationService',
        function($scope,UserOrderService,$state,ShoppingCartService,AuthenticationService){

            $scope.order = null;
            $scope.total = null;
            $scope.showModal = false;

            $scope.init = function(){

                // Get the order from our shopping cart
                $scope.order = ShoppingCartService.getShoppingCart();

                $scope.setTotal();

            };


            //Get the total at the beginning
            $scope.setTotal = function(){
                $scope.total = 0 ;

                angular.forEach($scope.order, function(value, key){
                    $scope.total += value.total;
                });

            }



            $scope.updateTotal = function(amount,productOrder){
                console.log("Product:"+productOrder+" Amount:"+amount);

                if(amount===""){
                    amount = 0;
                }

                var oldTotal = productOrder.total;
                //Input the new total to the product total
                productOrder.total = JSON.stringify((productOrder.product.price) * parseInt(amount));

                //Change the total for the order
                $scope.setTotal();

                //Change into order, save into the shopping cart service
                ShoppingCartService.saveOrder($scope.order);
            };

            $scope.send = function(){

                var user = {
                    "id" : AuthenticationService.currentUserId()
                }

                var order = ShoppingCartService.getShoppingCart();

                var sendData = {
                    "user":user,
                    "order":order
                }

                UserOrderService.send(sendData,
                function(){

                    //TODO: delete shopping cart
                    ShoppingCartService.delete();

                    //TODO: show successful message

                    $('#modal').modal('hide');

                },
                function(){
                    //TODO: show error
                });
            };



        }]);


})()