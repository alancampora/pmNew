(function(){
    'use strict';

    angular
        .module('app')
        .directive('showProducts', function() {
            return {
                restrict : 'E',
                templateUrl : "directives/showProducts.html",
                scope: {
                    products: "=",
                    add:"&"
                },
                link: function (scope, element, attrs) {

                    scope.addProduct = function(product,amount){
                        var func = scope.add();
                        func(product,amount);
                    };

                }

            }

        })

})()