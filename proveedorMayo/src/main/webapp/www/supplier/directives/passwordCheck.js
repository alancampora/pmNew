(function(){
    'use strict';

    angular
        .module('app')
        .directive('passwordCheck', function () {
            return {
                require: 'ngModel',
                link: function (scope, elem, attrs, ctrl) {
                    var firstPassword = '#' + attrs.passwordCheck;
                    elem.add(firstPassword).on('keyup', function () {
                        scope.$apply(function () {
                            // console.info(elem.val() === $(firstPassword).val());
                            ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                        });
                    });
                }
            }
        });
})();