(function(){
    'use strict';

    angular
        .module('app')
        .directive('productOrder', function() {
            return {
                restrict : 'E',
                templateUrl : "directives/productOrder.html",
                scope: {
                    order: "="
                },
                link: function(scope, element, attrs) {
                    scope.$watch('val', function(newValue, oldValue) {
                        if (newValue)
                            console.log("I see a data change!");
                    }, true);
                }

            }

        })

})()