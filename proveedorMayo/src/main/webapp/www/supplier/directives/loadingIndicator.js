(function(){
    'use strict';

    angular
        .module('app')
        .directive("loadingIndicator", function ($timeout) {
            return {
                restrict: "E",
                templateUrl : "directives/loadingIndicator.html",

                link: function (scope, element, attrs) {

                    scope.$on("loading-started", function (e) {
                            element.css({ "display": "inherit" });
                    });

                    scope.$on("loading-complete", function (e) {
                        element.css({ "display": "none" });
                    });

                }
            };
        });

})();