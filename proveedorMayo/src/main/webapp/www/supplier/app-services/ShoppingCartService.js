(function(){
    'use strict';

    angular
        .module('app')
        .factory('ShoppingCartService',[function() {

            var service = {};

            //Storing the following object
            /*
                shoppingCart:{
                    "IDXXXXX":{
                        amount: "xxxx"
                        product:{id,price,xxx,xxx}
                    },
                    "IDYYYYY":{
                        amount: "yyyy"
                        product:{id,price,yyy,yyyy}
                    },
                }
             */
            service.shoppingCart = null;

            service.init = function(){
                service.shoppingCart = localStorage['shoppingCart']!= null ? JSON.parse(localStorage['shoppingCart']) : {};
            }

            service.addProduct = function (product,amount) {

                var numberPrice = product.price;
                product.price = JSON.stringify(product.price);

                var obj = {
                        product: product,
                        amount: amount,
                        total: JSON.stringify(amount * numberPrice)

                }

                //Store product in local storage and in shoppingCart variable
                service._saveProduct(obj,product.id)
            };

            service.getShoppingCart = function(){
                return service.shoppingCart
            };

            service.delete = function(){
                delete localStorage["shoppingCart"]
            };

            service.saveOrder = function(order){
                localStorage['shoppingCart'] = JSON.stringify(order);
            };

            service._saveProduct = function(product,id){
                service.shoppingCart[id] = product ;
                localStorage['shoppingCart'] = JSON.stringify(service.shoppingCart);
            };



            return service;

        }])

})();