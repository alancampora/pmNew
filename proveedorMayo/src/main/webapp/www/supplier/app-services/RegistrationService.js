(function(){
    'use strict';

    angular
        .module('app')
        .factory('RegistrationService',['$http',function($http) {
            var service = {};


            service.registerPerson = function (value, successCallback, failCallback) {
                console.log("RegistrationService registerPerson value:" + value);
                service._registerUser(
                    value,
                    '/pmSupplier/app/registrationController/person',
                    successCallback,
                    failCallback);
            };

            service.registerCompany = function(value,successCallback, failCallback){
                console.log("RegistrationService registerCompany value:" + value);
                service._registerUser(
                    value,
                    '/pmSupplier/app/registrationController/company',
                    successCallback,
                    failCallback);
            };

            service._registerUser = function(value,url,successCallback,failCallback){

                $http({
                    method:'POST',
                    url: url,
                    params:{value:value}
                })
                    .success(function (data) {
                        console.log("SUCCESS" + data);
                        successCallback.call(this, data);
                    })

                    .error(function (data) {
                        failCallback.call(this, data);
                    });
            }

            return service;

        }])

})();