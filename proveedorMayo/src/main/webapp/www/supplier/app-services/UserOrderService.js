(function(){
    'use strict';

    angular
        .module('app')
        .factory('UserOrderService',['$http','$cookies',function($http,$cookies) {
            var service = {};


            service.send = function (order, successCallback, failCallback) {
                console.log("UserOrderService order:" + order);

                $http({
                    method:'POST',
                    url:'/pmSupplier/app/orderController/send',
                    params:{data:order}
                })
                    .success(function (data) {
                        console.log("SUCCESS" + data);

                        successCallback.call(this, data);
                    })
                    .error(function (data) {
                        failCallback.call(this, data);
                    });
            };

            return service;

        }])

})();