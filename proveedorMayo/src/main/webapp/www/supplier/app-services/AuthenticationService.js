(function(){
    'use strict';

    angular
        .module('app')
        .factory('AuthenticationService',['$http','$cookies',function($http,$cookies) {
            var service = {};


            service.login = function (user, password, successCallback, failCallback) {
                console.log("LoginService user:" + user);

                $http({
                    method:'GET',
                    url:'/pmSupplier/app/loginController/login',
                    params:{personalId:user,password:password}
                    })
                    .success(function (data) {
                        console.log("SUCCESS" + data.name);

                        service.setUserLoggedIn(data)

                        successCallback.call(this, data);
                    })

                    .error(function (data) {
                        failCallback.call(this, data);
                    });
            };

            service.logout = function(){
                $cookies['user.isLogged'] =  false;
                $cookies['user.id'] =  '';
                return;
            }

            service.setUserLoggedIn = function(data){
                $cookies['user.isLogged'] =  true;
                $cookies['user.id'] =  data.id;
            };

            service.currentUserId = function () {
                return $cookies['user.id'];
            };

            service.isLogged = function(){
                var val = ($cookies['user.isLogged'] === "true" || $cookies['user.isLogged'] === true);
                return val;
            }

            return service;

        }])

})();