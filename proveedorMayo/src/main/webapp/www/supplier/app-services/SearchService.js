(function(){
    'use strict';

    angular
        .module('app')
        .factory('SearchService',['$http',function($http) {
            var service = {};


            service.search = function (value, successCallback, failCallback) {
                console.log("SearchService value:" + value);

                $http({
                    method:'GET',
                    url:'/pmSupplier/app/searchController/search',
                    params:{value:value}
                    })
                    .success(function (data) {
                        console.log("SUCCESS" + data);
                        successCallback.call(this, data);
                    })

                    .error(function (data) {
                        failCallback.call(this, data);
                    });
            };

            service.currentUser = function () {
                return service.user;
            };

            return service;

        }])

})();