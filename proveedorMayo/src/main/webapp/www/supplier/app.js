(function(){

    'use strict';

var app = angular.module('app', ['ui.router','localization','ngCookies'])

    app.config(function($stateProvider,$urlRouterProvider){

        $urlRouterProvider.otherwise('home/main');

        //Defining children for our parents helps us to create the layout =D , FUCKING AWESOME LPM!!
        $stateProvider
            .state('home', {
                url:'/home',
                templateUrl: 'mvc/view/home.html',
                data: {
                    requireLogin: false
                }
            })
                .state('home.main', { // home/main
                    url:'/main',
                    templateUrl: 'mvc/view/home.main.html',
                })
                .state('home.login', {
                    url:'/login',
                    templateUrl: 'mvc/view/home.login.html',
                    controller: 'LoginController',
                })
                .state('home.speech',{
                    url:'/speech',
                    templateUrl: 'mvc/view/home.speech.html'
                })
                .state('home.registration',{
                    url:'/registration',
                    templateUrl: 'mvc/view/home.registration.html'
                })
                    .state('home.registration.person',{
                        url:'/person',
                        templateUrl: 'mvc/view/home.registration.person.html',
                        controller:'HomeRegistrationPersonController'
                    })
                    .state('home.registration.company',{
                        url:'/company',
                        templateUrl: 'mvc/view/home.registration.company.html',
                        controller:'HomeRegistrationCompanyController'
                    })

            .state('user', {
                url:'/user',
                templateUrl: 'mvc/view/user.html',
                controller:'UserController',
                data: {
                    requireLogin: true
                }
            })
                .state('user.catalog', {
                    url:'/catalog',
                    templateUrl: 'mvc/view/user.catalog.html',
                    controller:'CatalogController'

                })
                .state('user.order', {
                    url:'/order',
                    templateUrl: 'mvc/view/user.order.html',
                    controller:'UserOrderController'

                })

       });

    app.run(function ($rootScope,$state,AuthenticationService) {

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {

            var isLogged = AuthenticationService.isLogged(); //take care cookies store a string value
            var requireLogin = toState.data.requireLogin;

            if(!isLogged && requireLogin ){
                //Login View
                event.preventDefault(); //without this the $state.go wont work
                $state.go('home.login',{});
            }
        });
    });

    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push(function ($q, $rootScope) {
            return {
                'request': function (config) {

                    if (config.url.indexOf(".html") != -1 || config.url.indexOf(".js") != -1) {
                        return config;
                    }


                    $rootScope.$broadcast('loading-started');
                    return config || $q.when(config);
                },
                'response': function (response) {
                    $rootScope.$broadcast('loading-complete');
                    return response || $q.when(response);
                },
                'responseError': function (rejection) {
                    $rootScope.$broadcast('loading-complete');
                    return $q.reject(rejection);
                }
            };
        });
    });

})();



