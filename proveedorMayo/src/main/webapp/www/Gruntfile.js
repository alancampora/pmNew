'use strict';

module.exports = function(grunt) {

    grunt.initConfig({
        source:{
            deploy: '/Users/admin/tools/apache-tomcat-7/webapps/pmSupplier/www/supplier',
            supplier:'supplier'
        },
        lib:{
            source:'../lib',
            deploy:'/Users/admin/tools/apache-tomcat-7/webapps/pmSupplier/www/lib'
        },
        watch: {
            all: {
                files: ['<%= source.supplier %>/**/*'],
                tasks: ['default']
            },
            options: {
                liveReload:true
            }
        },
        copy: {
            dist:{
                files: [ {
                    expand: true,
                    cwd: '<%= source.supplier %>',
                    src: [
                        '**'
                    ],
                    dest: '<%= source.deploy %>'
                } ]
            },
            lib:{
                files: [ {
                    expand: true,
                    cwd: '<%= lib.source %>',
                    src: [
                        '**'
                    ],
                    dest: '<%= lib.deploy %>'
                } ]
            }

        },
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.registerTask('default', ['copy:dist','copy:lib']);
};