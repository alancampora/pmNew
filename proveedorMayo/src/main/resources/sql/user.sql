--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` mediumint(9) NOT NULL,
  `NOMBRE` text NOT NULL,
  `APELLIDO` text NOT NULL,
  `MAIL` text NOT NULL,
  `PASS` text NOT NULL,
  `TELEFONO` text NOT NULL,
  `RAZON_SOCIAL` text NOT NULL,
  `CUIT` text NOT NULL,
  `TIPO_IVA` text NOT NULL,
  `COD_POSTAL` text NOT NULL,
  `DIRECCION_COMPLETA` text NOT NULL,
  `LOCALIDAD` text NOT NULL,
  `PROVINCIA` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
